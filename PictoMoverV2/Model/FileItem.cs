﻿using System;
using System.ComponentModel;
using System.IO;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Microsoft.WindowsAPICodePack.Shell;

namespace PictoMoverV2.Model
{
    public class FileItem : INotifyPropertyChanged
    {
        public static int ThumbWidth = 100, ThumbHeight = 100;

        public event PropertyChangedEventHandler PropertyChanged;

        public string Path { get; }

        public DateTime DateTime { get; }

        public bool Keep { get; set; }

        public ImageSource Image { get; private set; }

        /// <summary>
        ///     Create a new file item with the given path. Will use lesser of M, C stamp for DateTime.
        /// </summary>
        /// <param name="path">Path to file item</param>
        public FileItem(string path)
        {
            Path = path;

            DateTime = File.GetLastWriteTime(Path) < File.GetCreationTime(Path) ?
                File.GetLastWriteTime(Path) : File.GetCreationTime(Path);

            Keep = true;

            GetThumb();
        }

        /// <summary>
        ///     Get shell thumb and stream to WPF bitmap
        /// </summary>
        private async void GetThumb()
        {
            await System.Threading.Tasks.Task.Run(() =>
            {
                using (MemoryStream outStream = new MemoryStream())
                {
                    BitmapImage outImage = new BitmapImage();

                    System.Drawing.Bitmap bitmap;
                    ShellFile shellFile = ShellFile.FromFilePath(Path);

                    try
                    {
                        bitmap = shellFile.Thumbnail.Bitmap;
                    }
                    catch
                    {
                        bitmap = shellFile.Thumbnail.Icon.ToBitmap();
                    }

                    double ratio = (double)bitmap.Width / bitmap.Height;

                    bitmap.Save(outStream, System.Drawing.Imaging.ImageFormat.Bmp);

                    bitmap.Dispose();
                    shellFile.Dispose();

                    outImage.BeginInit();
                    outStream.Seek(0, SeekOrigin.Begin);
                    outImage.StreamSource = outStream;
                    outImage.CacheOption = BitmapCacheOption.OnLoad;
                    outImage.DecodePixelHeight = ratio > 1 ? Convert.ToInt32(Math.Min(ThumbHeight, ThumbHeight / ratio)) : ThumbHeight;
                    outImage.DecodePixelWidth = ratio <= 1 ? Convert.ToInt32(Math.Min(ThumbWidth, ThumbWidth * ratio)) : ThumbWidth;
                    outImage.EndInit();

                    Image = outImage;
                    Image.Freeze();

                    OnPropertyChanged("Image");
                }
            });
        }

        protected void OnPropertyChanged(string name) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
    }
}
