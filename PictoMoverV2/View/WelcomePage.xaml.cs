﻿using System;
using System.Windows;

namespace PictoMoverV2.View
{
    /// <summary>
    /// Interaction logic for WelcomePage.xaml
    /// </summary>
    public partial class WelcomePage : NavigablePage
    {
        private Wizard _parent;

        public WelcomePage(Wizard parent)
        {
            _parent = parent ?? throw new ArgumentNullException(nameof(parent));
            InitializeComponent();
        }

        private void ButtonNext_Click(object sender, RoutedEventArgs e) => _parent.Navigate(target: new SettingsPage(parent: _parent, canCancel: false, target: new ReviewPage(parent: _parent)));
    }
}
