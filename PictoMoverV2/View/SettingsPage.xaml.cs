﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace PictoMoverV2.View
{
    /// <summary>
    /// Interaction logic for SettingsPage.xaml
    /// </summary>
    public partial class SettingsPage : NavigablePage
    {
        private Wizard _parent;
        private NavigablePage _target;
        private Dictionary<Control, bool> _validControls;
        private bool _dirty = false;

        public override bool AcceptNavigation => _dirty ? MessageBox.Show("Discard unsaved changes?", "Confirm Navigation", MessageBoxButton.OKCancel, MessageBoxImage.Warning) == MessageBoxResult.OK : true;

        public SettingsPage(Wizard parent, bool canCancel = true, NavigablePage target = null)
        {
            _parent = parent ?? throw new ArgumentNullException(nameof(parent));
            _target = target;

            InitializeComponent();
            ButtonCancel.IsEnabled = canCancel;

            _validControls = new Dictionary<Control, bool>()
            {
                { TextBoxSourcePath, false },
                { TextBoxDestinationPath, false },
                { TextBoxFolderNaming, false },
                { TextBoxExtensions, false }
            };

            Properties.Settings settings = Properties.Settings.Default;

            TextBoxSourcePath.Text = settings.sourcePath;
            TextBoxDestinationPath.Text = settings.destinationPath;
            TextBoxFolderNaming.Text = settings.folderNaming;
            TextBoxExtensions.Text = settings.extensions;
            CheckBoxDeleteOnMove.IsChecked = settings.deleteOnMove;
            CheckBoxWarnDelete.IsChecked = settings.showDeleteWarn;
            ForceValidate(hideWarn: true);

            _dirty = false;
        }

        private void ForceValidate(bool hideWarn = false)
        {
            TextBoxDestinationPath_LostFocus(this, null);
            TextBoxSourcePath_LostFocus(this, null);
            TextBoxFolderNaming_LostFocus(this, null);
            TextBoxExtensions_LostFocus(this, null);
            if (hideWarn)
            {
                ValidationLabelSource.Visibility =
                    ValidationLabelDestination.Visibility =
                    ValidationLabelExtensions.Visibility =
                    Visibility.Collapsed;
            }
        }

        private void HyperlinkDateTimeFormatHelp_Click(object sender, RoutedEventArgs e) => System.Diagnostics.Process.Start("https://docs.microsoft.com/en-us/dotnet/standard/base-types/standard-date-and-time-format-strings");

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            _dirty = false;
            _parent.GoBack();
        }

        private void ButtonAccept_Click(object sender, RoutedEventArgs e)
        {
            if (!_validControls.All(c => c.Value))
            {
                ForceValidate();
                MessageBox.Show("Please correct invalid fields.", "Invalid entries exist", MessageBoxButton.OK, MessageBoxImage.Stop);
                return;
            }

            Properties.Settings settings = Properties.Settings.Default;

            if (settings.firstRun)
                settings.firstRun = false;
            settings.sourcePath = TextBoxSourcePath.Text.Trim();
            settings.destinationPath = TextBoxDestinationPath.Text.Trim();
            settings.folderNaming = TextBoxFolderNaming.Text.Trim();
            settings.extensions = TextBoxExtensions.Text.Trim();
            settings.deleteOnMove = CheckBoxDeleteOnMove.IsChecked ?? false;
            settings.showDeleteWarn = CheckBoxWarnDelete.IsChecked ?? false;

            settings.Save();
            _dirty = false;

            if (_target == null)
                _parent.GoBack();
            else
                _parent.Navigate(_target);
        }

        /// <summary>
        ///     Perform validation on a path. A path is considered valid if it exists!
        /// </summary>
        /// <param name="textBox">Text box path was input in</param>
        /// <param name="validationLabel">Label to use for validation messages</param>
        /// <param name="validControls">Dictionary of validated controls</param>
        /// <param name="dirty">Dirty bit</param>
        private static void ValidatePath(ref TextBox textBox, ref Label validationLabel, ref Dictionary<Control, bool> validControls, ref bool dirty)
        {
            if (!System.IO.Directory.Exists(textBox.Text.Trim()))
            {
                validationLabel.Content = "Given directory does not exist";
                validationLabel.Visibility = Visibility.Visible;
                validControls[textBox] = false;
            }
            else
            {
                validationLabel.Visibility = Visibility.Collapsed;
                validControls[textBox] = true;
                dirty = true;
            }
        }

        /// <summary>
        ///     Browse for a folder
        /// </summary>
        /// <param name="textBox">Text box to place selected path in</param>
        /// <param name="description">Description to display in dialog</param>
        /// <param name="initialPath">Initial selected path</param>
        /// <param name="validationFunction">Delegate to refresh validation</param>
        /// <param name="dirty">Dirty bit</param>
        private static void DoBrowseFolder(ref TextBox textBox, string description, string initialPath, Delegate validationFunction, ref bool dirty)
        {
            System.Windows.Forms.FolderBrowserDialog folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog()
            {
                Description = description,
                RootFolder = Environment.SpecialFolder.Desktop,
                ShowNewFolderButton = true,
                SelectedPath = initialPath
            };

            switch (folderBrowserDialog.ShowDialog())
            {
                case System.Windows.Forms.DialogResult.OK:
                    textBox.Text = folderBrowserDialog.SelectedPath;
                    validationFunction.DynamicInvoke(null, null);
                    dirty = true;
                    return;
                case System.Windows.Forms.DialogResult.Cancel:
                    return;
                default:
                    MessageBox.Show("The folder browser dialog returned an unexpected result.", "Internal Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
            }
        }

        private delegate void ValidateTextBoxPath(object sender = null, RoutedEventArgs e = null);

        private void TextBoxSourcePath_LostFocus(object sender, RoutedEventArgs e) => ValidatePath(
            textBox: ref TextBoxSourcePath, 
            validationLabel: ref ValidationLabelSource, 
            validControls: ref _validControls, 
            dirty: ref _dirty);

        private void TextBoxDestinationPath_LostFocus(object sender, RoutedEventArgs e) => ValidatePath(
            textBox: ref TextBoxDestinationPath, 
            validationLabel: ref ValidationLabelDestination, 
            validControls: ref _validControls, 
            dirty: ref _dirty);

        private void TextBoxFolderNaming_LostFocus(object sender, RoutedEventArgs e)
        {
            if (TextBoxFolderNaming.Text.Length == 0)
            {
                ValidationLabelFolderNaming.Content = "Folder naming template cannot be empty.";
                ValidationLabelFolderNaming.Visibility = Visibility.Visible;
                ValidationLabelFolderNaming.Foreground = new SolidColorBrush(Colors.Red);
                _validControls[TextBoxFolderNaming] = false;
            }
            else
                _validControls[TextBoxFolderNaming] = true;
        }

        private void TextBoxExtensions_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBoxExtensions.Text = new string(TextBoxExtensions.Text.Trim().ToUpper().Where(c => char.IsLetterOrDigit(c) || c == ',' || c == '*').ToArray());

            if (TextBoxExtensions.Text.Length == 0)
            {
                ValidationLabelExtensions.Content = "You need at least AN extension, or we won't move anything!";
                ValidationLabelExtensions.Visibility = Visibility.Visible;
                _validControls[TextBoxExtensions] = false;
            }
            else
            {
                ValidationLabelExtensions.Visibility = Visibility.Collapsed;
                _validControls[TextBoxExtensions] = true;
            }
        }

        private void TextBoxFolderNaming_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (TextBoxFolderNaming.Text.Length == 0)
            {
                TextBoxFolderNaming_LostFocus(sender, null);
                return;
            }

            ValidationLabelFolderNaming.Visibility = Visibility.Visible;
            ValidationLabelFolderNaming.Foreground = new SolidColorBrush(Colors.Black);

            ValidationLabelFolderNaming.Content = string.Format("Example: '{0}'", DateTime.Now.ToString(TextBoxFolderNaming.Text));

            _dirty = true;
        }

        private void ButtonBrowseSource_Click(object sender, RoutedEventArgs e) => DoBrowseFolder(
                textBox: ref TextBoxSourcePath,
                description: "Select a folder to move pictures from.",
                initialPath: System.IO.Directory.Exists(TextBoxSourcePath.Text) ? TextBoxSourcePath.Text : Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                validationFunction: new ValidateTextBoxPath(TextBoxSourcePath_LostFocus),
                dirty: ref _dirty);

        private void ButtonBrowseDestination_Click(object sender, RoutedEventArgs e) => DoBrowseFolder(
                textBox: ref TextBoxDestinationPath, 
                description: "Select a folder to move pictures to.", 
                initialPath: System.IO.Directory.Exists(TextBoxDestinationPath.Text) ? TextBoxDestinationPath.Text : Environment.GetFolderPath(Environment.SpecialFolder.MyPictures), 
                validationFunction: new ValidateTextBoxPath(TextBoxDestinationPath_LostFocus), 
                dirty: ref _dirty);
    }
}
