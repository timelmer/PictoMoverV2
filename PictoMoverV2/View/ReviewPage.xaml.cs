﻿using System;
using System.Linq;
using System.Windows;
using System.IO;
using System.ComponentModel;
using System.Windows.Controls;
using PictoMoverV2.Model;
using System.Collections.Generic;
using Shell32;

namespace PictoMoverV2.View
{
    /// <summary>
    /// Interaction logic for ReviewPage.xaml
    /// </summary>
    public partial class ReviewPage : NavigablePage, IDisposable
    {
        private Wizard _parent;
        private Properties.Settings _settings = Properties.Settings.Default;
        private BindingList<FileItem> _fileItems = new BindingList<FileItem>();
        private Progress<int> _progress { get; }

        public System.Windows.Forms.BindingSource FileItems { get; } = new System.Windows.Forms.BindingSource();

        public ReviewPage(Wizard parent)
        {
            _parent = parent ?? throw new ArgumentNullException(nameof(parent));
            _progress = new Progress<int>(p => { ProgressBar.Value = p; });

            InitializeComponent();
            RegisterName("ReviewPage", this);

            FileItems.DataSource = _fileItems;
        }

        private void ButtonRefresh_Click(object sender, RoutedEventArgs e) => LoadImages();

        private void NavigablePage_Loaded(object sender, RoutedEventArgs e) => LoadImages();

        /// <summary>
        ///     Load images from source directory
        /// </summary>
        private void LoadImages()
        {
            try
            {
                string[] extensions = _settings.extensions.Split(',');
                IEnumerable<string> directory = Directory.EnumerateFiles(_settings.sourcePath, "*", SearchOption.AllDirectories);

                // Add new items
                foreach (string path in directory.Where(f => _fileItems.Count(i => i.Path.Equals(f)) == 0 && extensions.Contains(Path.GetExtension(f).Remove(0, 1).ToUpper())))
                    _fileItems.Add(new FileItem(path));

                // Remove extra items
                List<FileItem> diff = _fileItems.Where(i => !directory.Contains(i.Path)).ToList();
                for (int i = 0; i < diff.Count(); i++)
                    _fileItems.Remove(diff.ElementAt(i));
            }
            catch (Exception x)
            {
                MessageBox.Show(string.Format("An error occurred while loading image previews: {0}", x.Message), "Source Error", MessageBoxButton.OK, MessageBoxImage.Error);
                _parent.Navigate(new SettingsPage(parent: _parent, canCancel: false));
            }
            _fileItems.OrderBy(f => f.DateTime);
        }

        private void Image_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.ClickCount >= 2)
                System.Diagnostics.Process.Start((string)((Image)sender).Tag);
        }

        private async void ButtonStart_Click(object sender, RoutedEventArgs e)
        {
            if (_settings.showDeleteWarn && _fileItems.Any(f => !f.Keep) && _settings.deleteOnMove)
                switch (MessageBox.Show("Unchecked items will be sent to the Recycle Bin. Do you wish to continue? (You can turn this warning off in the settings)", "Confirm Deletion", MessageBoxButton.YesNo, MessageBoxImage.Warning))
                {
                    case MessageBoxResult.Yes:
                        break;
                    case MessageBoxResult.No:
                        return;
                }

            List<Tuple<string, Exception>> exceptions = new List<Tuple<string, Exception>>();
            Folder recycleBin = new Shell().NameSpace(ShellSpecialFolderConstants.ssfBITBUCKET);

            await System.Threading.Tasks.Task.Run(() =>
            {
                for (int i = 0; i < _fileItems.Count; i++)
                {
                    FileItem fileItem = _fileItems[i];

                    OrganizeFile(fileItem, ref exceptions, ref recycleBin);
                    ((IProgress<int>)_progress).Report(Convert.ToInt32(Math.Floor(((double)i + 1) / _fileItems.Count * 100)));
                }
            });

            if (exceptions.Count > 0)
                MessageBox.Show(string.Concat(exceptions.Select(x => string.Format("{0} : {1}", x.Item1, x.Item2.Message))), "Operation Completed With Errors", MessageBoxButton.OK, MessageBoxImage.Error);
            else
                switch(MessageBox.Show("Files have been organized. Would you like to close PM2?", "Operation Completed", MessageBoxButton.YesNo, MessageBoxImage.Question))
                {
                    case MessageBoxResult.Yes:
                        Environment.Exit(0);
                        break;  // Fake break to avoid compiler error
                    case MessageBoxResult.No:
                        return;
                }
        }

        /// <summary>
        ///     Organize file
        /// </summary>
        /// <param name="fileItem">File to organize</param>
        /// <param name="exceptions">List of filenames and exceptions</param>
        /// <param name="recycleBin">Recycle bin construct</param>
        private void OrganizeFile(FileItem fileItem, ref List<Tuple<string, Exception>> exceptions, ref Folder recycleBin)
        {
            if (fileItem.Keep)
                try
                {
                    string folder = Path.Combine(_settings.destinationPath, fileItem.DateTime.ToString(_settings.folderNaming));

                    if (!Directory.Exists(folder))
                        Directory.CreateDirectory(folder);
                    if (_settings.deleteOnMove)
                        File.Move(fileItem.Path, Path.Combine(folder, Path.GetFileName(fileItem.Path)));
                    else
                        File.Copy(fileItem.Path, Path.Combine(folder, Path.GetFileName(fileItem.Path)));
                }
                catch (Exception x)
                {
                    exceptions.Add(new Tuple<string, Exception>(Path.GetFileName(fileItem.Path), x));
                }
            else
                // Recycle file, suppress progress dialog
                recycleBin.MoveHere(fileItem.Path, new int[] { 4 });
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                FileItems.Dispose();
        }
    }
}
