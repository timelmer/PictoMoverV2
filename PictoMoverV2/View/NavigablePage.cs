﻿using System.Windows.Controls;

namespace PictoMoverV2.View
{
    /// <summary>
    ///     Abstraction of Page that allows pages to approve navigation
    /// </summary>
    public abstract class NavigablePage : Page
    {
        /// <summary>
        ///     Is navigation accepted by the page?
        /// </summary>
        public virtual bool AcceptNavigation => true;

        public NavigablePage() : base() { }
    }
}
