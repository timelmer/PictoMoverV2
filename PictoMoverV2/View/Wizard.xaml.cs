﻿using System;
using System.Windows;

namespace PictoMoverV2.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class Wizard : Window
    {
        public Wizard(bool reset = false)
        {
            InitializeComponent();
            if (reset && MessageBox.Show("ALL settings will be reset. Continue?", "Confirm Reset", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                Properties.Settings.Default.Reset();

            if (Properties.Settings.Default.firstRun)
                Navigate(new WelcomePage(parent: this));
            else
                Navigate(new ReviewPage(parent: this));
        }

        /// <summary>
        ///     Navigate to a page
        /// </summary>
        /// <param name="target">Page to navigate to</param>
        public void Navigate(NavigablePage target)
        {
            if (!WizardFrame.HasContent || ((NavigablePage)WizardFrame.Content).AcceptNavigation)
                WizardFrame.Navigate(target);
        }

        /// <summary>
        ///     Go back a page
        /// </summary>
        public void GoBack() => WizardFrame.GoBack();

        private void MenuSettings_Click(object sender, RoutedEventArgs e)
        {
            if (!Properties.Settings.Default.firstRun)
                Navigate(new SettingsPage(parent: this));
            else
                MessageBox.Show("Please follow the wizard.", "He won't lead you astray.", MessageBoxButton.OK, MessageBoxImage.Asterisk);
        }

        private void MenuExit_Click(object sender, RoutedEventArgs e) => Environment.Exit(exitCode: 0);

        private void MenuHelp_Click(object sender, RoutedEventArgs e) => new HelpWindow().Show();
    }
}
