﻿using System.Linq;
using System.Windows;

namespace PictoMoverV2
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        void App_Startup(object sender, StartupEventArgs e) =>
            new View.Wizard(reset: e.Args.Contains("/reset")).Show();
    }
}
