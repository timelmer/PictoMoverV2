PictoMover 2.0.0, Copyright Timothy Elmer 2018  
Icon by Lokas Software, www.awicons.com  
PictoMover is designed to facilitate the movement of pictures and video from a device (such as a phone or camera) or a cloud service to an organized folder on your computer.
# Use
Using PM2 is easy: set your source and destination as described in *Settings*, then deselect any files you don't want to keep in the Review page. You can also double-click on any file to open it in the defaut viewer.
# Settings
## Source Path
Defines where files will be moved/copied from. Files are copied recursively, but directories are flattened to the given source.
## Destination Path
Defines the root folder where files will be moved/copied to.
## Folder Naming Template
Files will be copied to organizational folders. This field defines the name of these folders and the organization of files. This field uses a DateTime format code, e.g. for May of '96 'yyyy-MM' -> '1996-05', 'yy-MMM' -> '96-May'.
## Extensions
File extensions to move/copy. Extensions should be listed **without** a period, and seperated by commas, e.g. 'JPG,MP4,CR2'.
## Delete Files After Move
Should PM2 delete files from the source once they have been moved?
## Warn Before Deleting Files
Should PM2 ask before deleting files?
## Where Did My Deleted Files Go?
If you unchecked them while reviewing photos, the Recycle Bin. If you are talking about the files from the source, they were **moved** to the destination, unless "Delete Files After Move" was unchecked.